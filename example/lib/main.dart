import 'package:bedux/bedux.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:launch_widget/launch_widget.dart';
import 'package:redux/redux.dart';
import 'package:rgv_logger/rgv_logger.dart';
import 'package:theme_switcher/theme_switcher.dart';
import 'package:toggler/toggler.dart';
import 'package:whereami/whereami.dart';

enum ThemeKey {
  light,
  dark,
}

final cupertinoThemes = {
  ThemeKey.light: CupertinoThemeData(
    brightness: Brightness.light,
  ),
  ThemeKey.dark: CupertinoThemeData(
    brightness: Brightness.dark,
  ),
};

final themes = {
  ThemeKey.light: ThemeData(
    brightness: Brightness.light,
  ),
  ThemeKey.dark: ThemeData(
    brightness: Brightness.dark,
  ),
};

enum Feature {
  x2,
}

const toggle = [
  FeatureToggle(Feature.x2, false, description: 'Increment by 2')
];

void main() {
  if (kReleaseMode) {
    Log.init(ReleaseLogger());
    Toggler.init(ReleaseToggler(toggle));
  } else {
    Log.init(DebugLogger());
    Toggler.init(InMemoryDebugToggler(toggle));
  }
  runApp(LaunchWidget<GlobalStore>(
    assemble: () => GlobalStore(AppState(0)),
    prepare: (context, store) => Future.value(), // init some stuff
    builder: (BuildContext context, GlobalStore store) => App(store: store),
  ));
}

class GlobalStore extends BXReduxStore<AppState> {
  GlobalStore(AppState initState)
      : super(Store(reducer, initialState: initState));
}

class App extends StatelessWidget {
  final BXStore store;

  const App({
    Key key,
    @required this.store,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialThemeSwitcher(
      initialKey: ThemeKey.dark,
      themes: themes,
      builder: (context) => MaterialApp(
        title: 'Flibs Demo',
        theme: Theme.of(context),
        home: HomePage(bloc: HomeBloc(store)),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  final HomeBloc bloc;

  const HomePage({Key key, @required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isDarkMode = MaterialThemeSwitcher.of(context).key == ThemeKey.dark;
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.all(24.0),
                  child:
                      Text('Running on ${describeEnum(WhereAmI.environment)}'),
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    GestureDetector(
                      onLongPress: () {
                        Log.i('Show toggler screen');
                        Toggler.show(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(24.0),
                        child:
                            Text('Hold this text to open feature toggles menu'),
                      ),
                    ),
                    Text('You have pushed the button this many times:'),
                    StreamBuilder<AppState>(
                        initialData: bloc.currentState(),
                        stream: bloc.stateUpdates(),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) return Container();
                          return Text(
                            '${snapshot.data.counter}',
                            style: Theme.of(context).textTheme.headline4,
                          );
                        }),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Switch(
                    value: isDarkMode,
                    onChanged: (value) {
                      final theme = value ? ThemeKey.dark : ThemeKey.light;
                      Log.i('Switch theme, now: ${describeEnum(theme)}');
                      MaterialThemeSwitcher.of(context).changeTheme(theme);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: bloc.increment,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

AppState reducer(AppState state, dynamic action) {
  if (action is IncrementAction) {
    Log.i('Incrementing by ${action.diff}', tags: ['redux']);
    return AppState(state.counter + action.diff);
  }
  return state;
}

class IncrementAction {
  final int diff;

  const IncrementAction(this.diff);
}

class AppState {
  final int counter;

  const AppState(this.counter);
}

class HomeBloc extends BXBloc<AppState> {
  HomeBloc(BXStore<AppState> store) : super(store);

  void increment() {
    final diff = Toggler.reader().isEnabled(Feature.x2) ? 2 : 1;
    dispatch(IncrementAction(diff));
  }
}
