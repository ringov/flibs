class Log {
  static Logger _logger;

  static void init(Logger logger) {
    if (Log._logger != null) {
      print('Logger has been already initialzed');
    }
    Log._logger = logger;
  }

  static void i(String message, {List<String> tags = const []}) =>
      _logger?.log(Severity.info, message: message, tags: tags);

  static void w(String message, {List<String> tags = const []}) =>
      _logger?.log(Severity.warning, message: message, tags: tags);

  static void e(
    Exception exception, {
    String message,
  }) =>
      _logger?.log(
        Severity.error,
        message: message,
        exception: exception,
      );

  const Log._();
}

abstract class Logger {
  void log(
    Severity severity, {
    List<String> tags = const [],
    String message,
    Exception exception,
  });
}

class ReleaseLogger implements Logger {
  @override
  void log(
    Severity severity, {
    List<String> tags = const [],
    String message,
    Exception exception,
  }) {
    // Log nothing in release
  }
}

class DebugLogger implements Logger {
  static String _pad(int value, {int width = 2}) =>
      value.toString().padLeft(width, '0');

  @override
  void log(
    Severity severity, {
    List<String> tags = const [],
    String message,
    Exception exception,
  }) {
    final now = DateTime.now();
    final timeFormatted = '${_pad(now.hour)}'
        ':${_pad(now.minute)}'
        ':${_pad(now.second)}'
        ':${_pad(now.millisecond, width: 3)}';
    print('$timeFormatted '
        '[${severity.toString().split('.')[1]}]'
        '${tags.isNotEmpty ? tags.toString() : ''}'
        '${message != null ? ' ' + message : ''}');
    if (exception != null) {
      print(exception);
    }
  }
}

enum Severity {
  info,
  warning,
  error,
}
