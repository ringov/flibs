class BXDiff<S> {
  final S lastState;
  final S newState;

  const BXDiff(this.lastState, this.newState);
}
