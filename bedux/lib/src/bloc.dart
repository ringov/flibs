import 'diff.dart';
import 'store.dart';

abstract class BXBloc<S> {
  final BXStore<S> _store;

  const BXBloc(this._store);

  void dispatch(dynamic action) => _store.dispatch(action);

  S currentState() => _store.currentState;

  Stream<S> stateUpdates({ShouldUpdateFilter<S> filter}) => filter != null
      ? _store.changes.where(filter).map((diff) => diff.newState)
      : _store.changes.map((diff) => diff.newState);
}

typedef ShouldUpdateFilter<S> = bool Function(BXDiff<S> diff);
