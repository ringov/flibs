import 'package:redux/redux.dart';

import 'diff.dart';

abstract class BXStore<S> {
  Stream<BXDiff<S>> get changes;

  S get currentState;

  void dispatch(dynamic action);
}

class BXReduxStore<S> implements BXStore<S> {
  final Store<S> store;
  S _lastState;

  BXReduxStore(this.store) : _lastState = store.state;

  @override
  void dispatch(dynamic action) => store.dispatch(action);

  @override
  Stream<BXDiff<S>> get changes => store.onChange.map((state) {
        final diff = BXDiff(_lastState, state);
        _lastState = state;
        return diff;
      });

  @override
  S get currentState => store.state;
}
