import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class BaseThemeSwitcher<T> extends StatefulWidget {
  final Object initialKey;
  final Map<Object, T> themes;
  final WidgetBuilder builder;

  const BaseThemeSwitcher({
    Key key,
    @required this.initialKey,
    @required this.themes,
    @required this.builder,
  }) : super(key: key);
}

abstract class BaseThemeSwitcherState<T, W extends BaseThemeSwitcher<T>>
    extends State<W> {
  Object _key;
  T _currentData;

  T get theme => _currentData;

  Object get key => _key;

  @override
  void initState() {
    _key = widget.initialKey;
    _currentData = widget.themes[_key];
    super.initState();
  }

  void changeTheme(Object key) {
    setState(() {
      _key = key;
      _currentData = widget.themes[key];
    });
  }
}

class _BaseThemeSwitcher<T, W extends BaseThemeSwitcher<T>,
S extends BaseThemeSwitcherState<T, W>> extends InheritedWidget {
  final S state;

  _BaseThemeSwitcher({
    Key key,
    @required this.state,
    @required WidgetBuilder builder,
  }) : super(
    key: key,
    child: Builder(builder: builder),
  );

  @override
  bool updateShouldNotify(_BaseThemeSwitcher oldWidget) => true;
}

typedef OnThemeChanged = Function(Object key);

class MaterialThemeSwitcher extends BaseThemeSwitcher<ThemeData> {
  static MaterialThemeSwitcherState of(BuildContext context) =>
      context
          .dependOnInheritedWidgetOfExactType<_MaterialThemeSwitcher>()
          .state;

  const MaterialThemeSwitcher({
    Key key,
    @required Object initialKey,
    @required Map<Object, ThemeData> themes,
    @required WidgetBuilder builder,
  }) : super(
    key: key,
    initialKey: initialKey,
    themes: themes,
    builder: builder,
  );

  @override
  State<MaterialThemeSwitcher> createState() => MaterialThemeSwitcherState();
}

class MaterialThemeSwitcherState
    extends BaseThemeSwitcherState<ThemeData, MaterialThemeSwitcher> {
  @override
  Widget build(BuildContext context) {
    return _MaterialThemeSwitcher(
      state: this,
      builder: (context) =>
          Theme(data: _currentData, child: Builder(builder: widget.builder)),
    );
  }
}

class _MaterialThemeSwitcher extends _BaseThemeSwitcher<ThemeData,
    MaterialThemeSwitcher,
    MaterialThemeSwitcherState> {
  _MaterialThemeSwitcher({
    Key key,
    @required MaterialThemeSwitcherState state,
    @required WidgetBuilder builder,
  }) : super(
    key: key,
    state: state,
    builder: builder,
  );
}

class CupertinoThemeSwitcher extends BaseThemeSwitcher<CupertinoThemeData> {
  static CupertinoThemeSwitcherState of(BuildContext context) =>
      context
          .dependOnInheritedWidgetOfExactType<_CupertinoThemeSwitcher>()
          .state;

  const CupertinoThemeSwitcher({
    Key key,
    @required Object initialKey,
    @required Map<Object, CupertinoThemeData> themes,
    @required WidgetBuilder builder,
  }) : super(
    key: key,
    initialKey: initialKey,
    themes: themes,
    builder: builder,
  );

  @override
  State<StatefulWidget> createState() => CupertinoThemeSwitcherState();
}

class CupertinoThemeSwitcherState
    extends BaseThemeSwitcherState<CupertinoThemeData, CupertinoThemeSwitcher> {
  @override
  Widget build(BuildContext context) {
    return _CupertinoThemeSwitcher(
      state: this,
      builder: (context) =>
          CupertinoTheme(
              data: _currentData, child: Builder(builder: widget.builder)),
    );
  }
}

class _CupertinoThemeSwitcher extends _BaseThemeSwitcher<CupertinoThemeData,
    CupertinoThemeSwitcher,
    CupertinoThemeSwitcherState> {
  _CupertinoThemeSwitcher({
    Key key,
    @required CupertinoThemeSwitcherState state,
    @required WidgetBuilder builder,
  }) : super(
    key: key,
    state: state,
    builder: (context) =>
        CupertinoTheme(data: state.theme, child: builder(context)),
  );
}
