import 'package:flutter/material.dart';

/// the entire file can be moved to a separate module

abstract class Toggler implements TogglerReader, TogglerUpdater {
  static Toggler _toggler;

  static TogglerReader reader() {
    if (_toggler == null) {
      throw Exception('You must call $Toggler.init() method first');
    }
    return _toggler;
  }

  static void init(Toggler provider) {
    if (_toggler != null) {
      print('$Toggler has been already initialized');
      return;
    }
    _toggler = provider;
  }

  static void show(BuildContext context) => _toggler.showPage(context);
}

class FeatureToggle {
  final Object id;
  final String description;
  final bool defaultValue;

  const FeatureToggle(
    this.id,
    this.defaultValue, {
    this.description,
  });
}

abstract class TogglerReader {
  List<FeatureToggle> get featureToggles;

  bool isEnabled(Object id);
}

abstract class TogglerUpdater {
  Future<void> update(Object id, bool isEnabled);

  void showPage(BuildContext context);
}

class InMemoryDebugToggler implements Toggler {
  @override
  List<FeatureToggle> featureToggles;

  final _toggles = <Object, bool>{};

  InMemoryDebugToggler(this.featureToggles);

  @override
  bool isEnabled(Object id) => _toggles[id] ?? false;

  @override
  void showPage(BuildContext context) => Navigator.of(context).push(
      MaterialPageRoute(builder: (context) => TogglerPage(provider: this)));

  @override
  Future<void> update(Object id, bool isEnabled) {
    if (featureToggles.where((element) => element.id == id).isNotEmpty) {
      _toggles[id] = isEnabled;
    } else {
      print('Warning: trying to update unknown feature toggle, id = $id');
    }
    return Future.value();
  }
}

class ReleaseToggler implements Toggler {
  ReleaseToggler(this.featureToggles);

  @override
  List<FeatureToggle> featureToggles;

  /// Always using default values in release provider
  @override
  bool isEnabled(Object id) =>
      featureToggles.firstWhere((feature) => feature.id == id,
          orElse: () => null)?.defaultValue ??
      false;

  /// Do not change feature toggles in release provider
  @override
  Future<void> update(Object id, bool isEnabled) => Future.value();

  /// Do not open any debug page in release mode
  @override
  void showPage(BuildContext context) {}
}

class TogglerPage extends StatefulWidget {
  final Toggler provider;

  const TogglerPage({
    Key key,
    @required this.provider,
  })  : assert(provider != null),
        super(key: key);

  @override
  _TogglerPageState createState() => _TogglerPageState();
}

class _TogglerPageState extends State<TogglerPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Toggler'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.replay),
            onPressed: () async {
              for (final feature in widget.provider.featureToggles) {
                await widget.provider.update(feature.id, feature.defaultValue);
              }
              setState(() {});
            },
          ),
        ],
      ),
      body: ListView.builder(
          itemCount: widget.provider.featureToggles.length,
          itemBuilder: (context, index) =>
              _buildItem(widget.provider.featureToggles[index])),
    );
  }

  Widget _buildItem(FeatureToggle item) => SwitchListTile(
        title: Text(item.description ?? item.id.toString()),
        subtitle: item.description != null ? Text(item.id.toString()) : null,
        value: widget.provider.isEnabled(item.id),
        onChanged: (bool value) async {
          await widget.provider.update(item.id, value);
          setState(() {});
        },
      );
}
