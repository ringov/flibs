import 'package:flutter/widgets.dart';

import 'di_widget.dart';
import 'init_widget.dart';

class LaunchWidget<D> extends StatefulWidget {
  final ReadyWidgetBuilder<D> builder;
  final WidgetBuilder waitingBuilder;
  final InitDependenciesCallback<D> assemble;
  final PrepareCallback<D> prepare;

  const LaunchWidget({
    Key key,
    @required this.builder,
    this.waitingBuilder,
    this.assemble,
    this.prepare,
  }) : super(key: key);

  @override
  _LaunchWidgetState<D> createState() => _LaunchWidgetState<D>();
}

class _LaunchWidgetState<D> extends State<LaunchWidget<D>> {
  D _dependencies;

  @override
  void initState() {
    super.initState();
    if (widget.assemble != null && _dependencies == null) {
      _dependencies = widget.assemble();
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return DI<D>(
      builder: (context) => InitWidget(
        init: () => widget?.prepare?.call(context, _dependencies),
        builder: (context, isInit) => isInit
            ? widget.builder(context, _dependencies)
            : widget.waitingBuilder != null
                ? widget.waitingBuilder(context)
                : Container(),
      ),
      dps: _dependencies,
    );
  }
}

typedef InitDependenciesCallback<D> = D Function();

typedef ReadyWidgetBuilder<D> = Widget Function(
    BuildContext context, D dependencies);

typedef PrepareCallback<D> = Future<void> Function(
    BuildContext context, D dependencies);
