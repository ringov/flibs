import 'package:flutter/widgets.dart';

class InitWidget extends StatefulWidget {
  final InitWidgetBuilder builder;
  final InitCallback init;

  const InitWidget({
    Key key,
    @required this.builder,
    @required this.init,
  }) : super(key: key);

  @override
  _InitWidgetState createState() => _InitWidgetState();
}

class _InitWidgetState extends State<InitWidget> {
  bool _initialized = false;

  @override
  void initState() {
    super.initState();
    _init();
  }

  Future<void> _init() async {
    if (!_initialized) {
      await widget.init();
      setState(() {
        _initialized = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) => widget.builder(context, _initialized);
}

typedef InitCallback = Future<void> Function();

typedef InitWidgetBuilder = Widget Function(
    BuildContext context, bool isInitialized);
