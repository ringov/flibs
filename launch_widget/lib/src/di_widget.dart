import 'package:flutter/widgets.dart';

class DI<D> extends InheritedWidget {
  static DI<D> of<D>(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<DI<D>>();

  /// Dependencies
  final D dps;

  DI({
    Key key,
    @required this.dps,
    @required WidgetBuilder builder,
  }) : super(key: key, child: Builder(builder: builder));

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
