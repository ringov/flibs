import 'dart:io';

import 'package:flutter/foundation.dart';

class WhereAmI {
  static final environment = _getEnvironment();

  static WAIEnv _getEnvironment() {
    if (kIsWeb) {
      return WAIEnv.web;
    } else {
      if (Platform.isAndroid) {
        return WAIEnv.android;
      } else if (Platform.isIOS) {
        return WAIEnv.ios;
      } else if (Platform.isMacOS) {
        return WAIEnv.mac;
      } else if (Platform.isLinux) {
        return WAIEnv.linux;
      } else if (Platform.isWindows) {
        return WAIEnv.windows;
      } else if (Platform.isFuchsia) {
        return WAIEnv.fuchsia;
      } else {
        return WAIEnv.unknown;
      }
    }
  }

  WhereAmI._();
}

enum WAIEnv {
  android,
  ios,
  web,
  mac,
  windows,
  linux,
  fuchsia,
  unknown,
}
