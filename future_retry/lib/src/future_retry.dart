extension RetryFuture<T> on Future<T> {
  Future<T> retry({
    FutureRetryOptions options,
  }) {
    return retryFuture(this, options: options);
  }
}

Future<T> retryFuture<T>(
  Future<T> future, {
  FutureRetryOptions options = const FutureRetryOptions(),
}) async {
  var counter = 0;
  var delay = options.firstRetryDelay;
  while (options.retryLimit == null || counter <= options.retryLimit) {
    if (counter != 0) {
      delay = options.delayFunction(delay);
      await Future.delayed(delay);
    }
    counter++;
    try {
      return await future;
    } catch (e) {
      if (options.retryLimit != null && counter > options.retryLimit) {
        rethrow;
      }
      if (!options.predicate(e)) {
        rethrow;
      }
    }
  }
  throw Exception('Retry function failed to retry');
}

class FutureRetryOptions {
  final Duration Function(Duration) delayFunction;
  final bool Function(Exception) predicate;
  final int retryLimit;
  final Duration firstRetryDelay;

  const FutureRetryOptions({
    this.delayFunction = exponentialRetry,
    this.predicate = retryOnAny,
    this.firstRetryDelay = const Duration(seconds: 1),
    this.retryLimit,
  });
}

Duration exponentialRetry(Duration currentDelay) =>
    Duration(milliseconds: currentDelay.inMilliseconds * 2);

bool retryOnAny(Exception e) => true;
